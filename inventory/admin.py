from django.contrib import admin

from .models import InventoryModel


class InventoryAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


admin.site.register(InventoryModel, InventoryAdmin)
