from inventory.api.views import InventoryView
from rest_framework import routers

inventory_router = routers.DefaultRouter(trailing_slash=True)
inventory_router.register(prefix='inventory', basename='inventory', viewset=InventoryView)
