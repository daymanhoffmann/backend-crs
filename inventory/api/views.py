from rest_framework.viewsets import ModelViewSet
from inventory.api.serializers import InventorySerializer
from inventory.models import InventoryModel


class InventoryView(ModelViewSet):
    serializer_class = InventorySerializer
    queryset = InventoryModel.objects.all()
