from django.db import models
from rate.models import RateModel


class InventoryModel(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=2)
    allotment = models.IntegerField()
    date = models.DateField(verbose_name='Inventory Date', db_index=True)
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Creation Date')
    updated = models.DateTimeField(auto_now=True, verbose_name='update date')
    rate_id = models.ForeignKey(RateModel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Inventory'
        verbose_name_plural = 'Inventories'
        db_table = 'inventory'

    def __str__(self):
        return (f'Date: {self.date}, Price: {self.price}€ ------ '
                f'{self.rate_id} -> '
                f'{self.rate_id.room_id} -> '
                f'{self.rate_id.room_id.hotel_id.name}')
