from django.db import models
from room.models import RoomModel
from backend_crs.utils import Utils


def custom_id():
    return Utils.build_custom_id(RateModel)


class RateModel(models.Model):
    prefix = 'rate_code_'
    name = models.CharField(max_length=200, null=False)
    id = models.CharField(primary_key=True, max_length=50, unique=True,
                          default=custom_id)
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Creation Date')
    updated = models.DateTimeField(auto_now=True, verbose_name='update date')
    room_id = models.ForeignKey(RoomModel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Rate'
        verbose_name_plural = 'Rates'
        db_table = 'rate'

    def __str__(self):
        return f'{self.id}, {self.name}'
