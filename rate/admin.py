from django.contrib import admin

from .models import RateModel


class RateAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


admin.site.register(RateModel, RateAdmin)
