from rest_framework import serializers
from rate.models import RateModel


class RateSerializer(serializers.ModelSerializer):

    class Meta:
        model = RateModel
        fields = '__all__'
