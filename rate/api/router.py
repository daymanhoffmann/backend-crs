from rate.api.views import RateView
from rest_framework import routers

rate_router = routers.DefaultRouter(trailing_slash=True)
rate_router.register(prefix='rate', basename='rate', viewset=RateView)
