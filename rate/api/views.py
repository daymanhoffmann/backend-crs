from rest_framework.viewsets import ModelViewSet
from rate.api.serializers import RateSerializer
from rate.models import RateModel


class RateView(ModelViewSet):
    serializer_class = RateSerializer
    queryset = RateModel.objects.all()
