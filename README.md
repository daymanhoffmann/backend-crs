# backend_crs

## Instalación del proyecto
- pipenv install
- pipenv install --dev
- pipenv shell

### Para ver ruta de pipenv. Util para configurar pycharm
- pipenv --venv

#### He subido el fichero de la BBDD en sqlite por lo que no es necesario hacer esto para arrancar el proyecto
- python manage.py makemigrations
- python manage.py migrate

## START
- python manage.py runserver

### Credenciales Admin
- http://127.0.0.1:8000/admin/
- user: admin
- pass: admin

## URLs de prueba
- http://127.0.0.1:8000/api/availability/hotel_code_1/2022-01-01/2022-01-03/
  - Como he supuesto que una habitacion puede tener tarifas distintas en funcion de los dias, he añadido un total por habitación
- http://127.0.0.1:8000/api/hotels/hotel_code_1/
- http://127.0.0.1:8000/api/hotels/

## DOCUMENTACION DE LA API
#### SWAGGER - http://127.0.0.1:8000/swagger/
#### REDOC - http://127.0.0.1:8000/redoc/#tag/hotels

## STOP
crtl+c

## Pyplint
Para utilizar pylint configurar pychar como especifica el siguiente link y luego hacer:
- link: https://stackoverflow.com/questions/38134086/how-to-run-pylint-with-pycharm/46409649#46409649
- click derecho en un fichero
- External Tools -> pylin

  - Faltan hacer correcciones en la mayoria de los ficheros. Lo adecuado seria que llegue a una puntuacion de 7/10


## TESTS UNITARIOS
- Ejecutar en la raiz del proyecto: pytest 

## TEST DE INTEGRACION
- No se han implementado test de integracion. Para esta practica seria interesante añadir este tipo de tests la consulta de availability y el flujo de los datos. Tambien en la parte de creacion de un inventory para ver como se crean correctamente las relaciones entre las distintas tablas
