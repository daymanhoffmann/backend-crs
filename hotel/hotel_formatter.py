class HotelFormatter:

    @classmethod
    def format_hotel_list(cls, data):
        return {'hotels': data.values_list('id')}

    @classmethod
    def format_availability_response(cls, rooms_dict):
        result_dict = {'room': []}
        for room, values in rooms_dict.items():
            rates = {'rates': []}
            for rate, rate_values in values.items():
                rates['rates'].append({rate: rate_values})
            result_dict['room'].append({room: rates})
        return result_dict
