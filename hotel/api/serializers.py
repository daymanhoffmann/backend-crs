from rest_framework import serializers
from hotel.models import HotelModel
from backend_crs.utils import Utils
from backend_crs.settings import DATE_INPUT_FORMAT


class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = HotelModel
        fields = '__all__'


class HotelsSerializer(serializers.ModelSerializer):
    class Meta:
        model = HotelModel
        fields = ('id',)


class DispoSerializer(serializers.Serializer):
    hotel_id = serializers.CharField(max_length=50, allow_null=False,
                                     allow_blank=False, required=True)
    start_date = serializers.DateField(input_formats=DATE_INPUT_FORMAT)
    end_date = serializers.DateField(input_formats=DATE_INPUT_FORMAT)

    def validate_hotel_id(self, data):
        prefix = HotelModel.prefix

        correct_prefix = data.startswith(prefix)
        try:
            if correct_prefix:
                sufix = data.split(prefix)[1]
                correct_sufix = Utils.check_num(sufix)
                if correct_sufix:
                    return data
        except:
            pass

        raise serializers.ValidationError(
            f'Error: wrong id. Format expected: hotel_code_1, Sent: {data}'
        )
