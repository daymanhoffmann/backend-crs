from hotel.api.views import HotelView
from rest_framework import routers
from django.urls import path, register_converter
from backend_crs.utils import DateUtils


hotel_router = routers.DefaultRouter(trailing_slash=True)
hotel_router.register(prefix='hotels', basename='hotels', viewset=HotelView)

register_converter(DateUtils, 'date')

urlpatterns = [
    path('availability/<slug:hotel_id>/<date:start_date>/<date:end_date>/',
         HotelView.get_availability, name='availability-section'),
]
