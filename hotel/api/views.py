from rest_framework import status as status_framework
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from django.http import JsonResponse

from hotel.hotel_provider import HotelProvider
from hotel.models import HotelModel
from hotel.api.serializers import HotelSerializer


class HotelView(ModelViewSet):
    serializer_class = HotelSerializer
    queryset = HotelModel.objects.all()

    def create(self, request, *args, **kwargs):
        return Response(HotelProvider.create(data=request.data),
                        status=status_framework.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        return Response(HotelProvider.hotels_list(data=request.query_params),
                        status=status_framework.HTTP_200_OK)

    def retrieve(self, request, *args, **kwargs):
        return Response(HotelProvider.list(data=kwargs),
                        status=status_framework.HTTP_200_OK)

    @classmethod
    def get_availability(cls, request, *args, **kwargs):
        values = HotelProvider.get_hotel_availability(kwargs)
        return JsonResponse(values, safe=False)
