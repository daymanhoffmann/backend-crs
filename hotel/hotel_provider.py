from .api.serializers import (HotelSerializer, HotelsSerializer,
                              DispoSerializer)
from room.room_provider import RoomProvider
from .models import HotelModel
from .hotel_formatter import HotelFormatter


class HotelProvider:

    @staticmethod
    def list(data):
        allowed_fields = ['pk']
        search = dict()
        for key, value in data.items():
            if key in allowed_fields:
                search.update({key: value})
        data = HotelModel.objects.filter(**search)
        return HotelSerializer(data, many=True).data

    @staticmethod
    def hotels_list(data):
        allowed_fields = ['pk']
        search = dict()
        for key, value in data.items():
            if key in allowed_fields:
                search.update({key: value})
        data = HotelModel.objects.filter(**search)
        return HotelFormatter.format_hotel_list(data)

    @staticmethod
    def create(data):
        serializer = HotelSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer.data

    @staticmethod
    def get_hotel_availability(data):
        serializer = DispoSerializer(data=data, context=data)
        if serializer.is_valid():
            rooms_dict = RoomProvider.get_availability_rooms(data['hotel_id'],
                                                             data['start_date'],
                                                             data['end_date'])
            return HotelFormatter.format_availability_response(rooms_dict)

        return serializer.errors
