from django.shortcuts import render
from rest_framework import status as status_framework
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from .hotel_provider import HotelProvider
from .serializers import HotelSerializer
from .models import HotelModel
from rest_framework.decorators import action


class HotelView(ModelViewSet):
    serializer_class = HotelSerializer
    queryset = HotelModel.objects.all()

    def create(self, request, *args, **kwargs):
        return Response(HotelProvider.create(data=request.data),
                        status=status_framework.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        return Response(HotelProvider.list(data=request.query_params),
                        status=status_framework.HTTP_200_OK)

    @action(detail=False, url_path='report', name='print-report')
    def report(self, request, **kwargs):
        return Response(f'Hello World: {request.data}')
