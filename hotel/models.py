from django.db import models
from backend_crs.utils import Utils


def custom_id():
    return Utils.build_custom_id(HotelModel)


class HotelModel(models.Model):
    prefix = 'hotel_code_'
    name = models.CharField(max_length=200, null=False)
    id = models.CharField(primary_key=True, max_length=50, unique=True,
                          default=custom_id)
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Creation Date')
    updated = models.DateTimeField(auto_now=True, verbose_name='update date')

    class Meta:
        verbose_name = 'Hotel'
        verbose_name_plural = 'Hotels'
        db_table = 'hotel'

    def __str__(self):
        return self.name
