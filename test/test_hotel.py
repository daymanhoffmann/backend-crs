import pytest
from hotel.models import HotelModel


@pytest.mark.django_db
def test_hotel_creation():
    hotel = HotelModel.objects.create(
        name='Hotel Test',
    )

    assert hotel.id == 'hotel_code_1'
    assert hotel.name == 'Hotel Test'
