from django.contrib import admin
from django.urls import path, include
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from inventory.api.router import inventory_router
from rate.api.router import rate_router
from room.api.router import room_router
from hotel.api.router import hotel_router


schema_view = get_schema_view(
    openapi.Info(
        title="Hotel API",
        default_version='v1',
        description="Test description",
        terms_of_service="https://.roiback.com/",
        contact=openapi.Contact(email="hofffmannn@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    # permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('api/', include(hotel_router.urls)),
    path('api/', include('hotel.api.router')),
    path('api/', include(inventory_router.urls)),
    path('api/', include(rate_router.urls)),
    path('api/', include(room_router.urls)),
    path('admin/', admin.site.urls),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0),
         name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0),
         name='schema-redoc'),
]
