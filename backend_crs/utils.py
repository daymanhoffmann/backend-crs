from re import match
from datetime import datetime, timedelta
from backend_crs.settings import DATE_INPUT_FORMAT


class Utils:
    num_pattern = "^\\d+$"

    @classmethod
    def build_custom_id(cls, model):
        last = model.objects.order_by('-id').first()
        prefix = model.prefix
        if not last:
            return prefix + '1'
        next_id = Utils.get_num_from_custom_id(last.id, prefix) + 1
        return prefix + str(next_id)

    @classmethod
    def get_num_from_custom_id(cls, custom_id, prefix):
        return int(custom_id.split(prefix)[1])

    @classmethod
    def check_num(cls, text):
        return match(Utils.num_pattern, text)


class DateUtils:
    regex = '\d{4}-\d{1,2}-\d{1,2}'

    def to_python(self, value):
        return datetime.strptime(value, DATE_INPUT_FORMAT).date()

    def to_url(self, value):
        return value.strftime(DATE_INPUT_FORMAT)

    @classmethod
    def to_str(cls, value):
        return value.strftime(DATE_INPUT_FORMAT)

    @classmethod
    def daterange(cls, start_date, end_date):
        for n in range(int((end_date - start_date).days)):
            yield start_date + timedelta(n)
