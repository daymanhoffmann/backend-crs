from rest_framework.viewsets import ModelViewSet
from room.api.serializers import RoomSerializer
from room.models import RoomModel


class RoomView(ModelViewSet):
    serializer_class = RoomSerializer
    queryset = RoomModel.objects.all()


