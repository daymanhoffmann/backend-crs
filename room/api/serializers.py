from rest_framework import serializers
from room.models import RoomModel


class RoomSerializer(serializers.ModelSerializer):

    class Meta:
        model = RoomModel
        fields = '__all__'
