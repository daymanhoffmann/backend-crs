from room.api.views import RoomView
from rest_framework import routers

room_router = routers.DefaultRouter(trailing_slash=True)
room_router.register(prefix='room', basename='room', viewset=RoomView)
