from django.db import models
from hotel.models import HotelModel
from backend_crs.utils import Utils


def custom_id():
    return Utils.build_custom_id(RoomModel)


class RoomModel(models.Model):
    prefix = 'room_code_'
    name = models.CharField(max_length=200, null=False)
    id = models.CharField(primary_key=True, max_length=50, unique=True,
                          default=custom_id)
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Creation Date')
    updated = models.DateTimeField(auto_now=True, verbose_name='update date')
    hotel_id = models.ForeignKey(HotelModel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Room'
        verbose_name_plural = 'Rooms'
        db_table = 'room'

    def __str__(self):
        return f'{self.id}, {self.name}'
