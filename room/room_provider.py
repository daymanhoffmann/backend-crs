from .models import RoomModel
from collections import defaultdict
from backend_crs.utils import DateUtils


class RoomProvider:
    @staticmethod
    def __search_dispo(hotel_id, start_date, end_date):
        queryset = RoomModel.objects.filter(
            hotel_id=hotel_id,
            ratemodel__inventorymodel__date__range=[start_date, end_date],
            ratemodel__inventorymodel__allotment__gt=0,
        )
        return list(queryset.values(
            'id', 'ratemodel__id', 'ratemodel__inventorymodel__allotment',
            'ratemodel__inventorymodel__price',
            'ratemodel__inventorymodel__date'))

    @staticmethod
    def __get_availabilities_rooms_ids(data, start_date, end_date):
        """
        Ensure for each room if its has availability by queried days.
        Returns a list of room ids that meet this condition
        """
        validator_dict = defaultdict(lambda: {})
        for r in data:
            str_date = DateUtils.to_str(
                r['ratemodel__inventorymodel__date'])
            validator_dict[r['id']][str_date] = True

        availabilities_rooms_ids = []
        for room in validator_dict:
            room_has_dispo = True
            for d in DateUtils.daterange(start_date, end_date):
                str_date = DateUtils.to_str(d)
                if not validator_dict[room].get(str_date):
                    room_has_dispo = False
                    break
            if room_has_dispo:
                availabilities_rooms_ids.append(room)
        return availabilities_rooms_ids

    @staticmethod
    def __build_availability_rooms_dict(rooms_id, result):
        """
        Buid dictionary like this:
        {
            'room_code_1': {
                'rate_code_3': {
                    'total_price': 20,
                    'breakdown': [
                        {
                            '2022-01-01': {
                                'price': 20,
                                'allotment': 3
                            }
                        }
                    ]
                }
            }
        }
        """
        # availability_dict
        av_dict = defaultdict(
            lambda: defaultdict(
                lambda: {
                    'total_price': 0,
                    'breakdown': []
                }))

        for room in result:
            room_id = room['id']
            if room_id not in rooms_id:
                continue
            inventory_date = room['ratemodel__inventorymodel__date']
            str_date = DateUtils.to_str(inventory_date)
            price = room['ratemodel__inventorymodel__price']
            allotment = room['ratemodel__inventorymodel__allotment']
            rate_id = room['ratemodel__id']

            av_dict[room_id][rate_id]['total_price'] += price
            av_dict[room_id]['total_price_room'] = av_dict[room_id].setdefault(
                'total_price_room', 0) + price

            inv = {
                str_date: {
                    'price': price,
                    'allotment': allotment
                }
            }
            av_dict[room_id][rate_id]['breakdown'].append(inv)

        return av_dict

    @staticmethod
    def get_availability_rooms(hotel_id, start_date, end_date):
        """
        Get dictionary with rooms available by param dates
        """
        result = RoomProvider.__search_dispo(hotel_id, start_date, end_date)
        rooms_ids = RoomProvider.__get_availabilities_rooms_ids(result,
                                                                start_date,
                                                                end_date)
        return RoomProvider.__build_availability_rooms_dict(rooms_ids, result)
