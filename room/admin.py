from django.contrib import admin

from .models import RoomModel


class RoomAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


admin.site.register(RoomModel, RoomAdmin)
